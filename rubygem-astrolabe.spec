%global gem_name astrolabe

Name: rubygem-%{gem_name}
Version: 1.3.0
Release: 1%{?dist}
Summary: An object-oriented AST extension for Parser
Group: Development/Languages
License: MIT
URL: https://github.com/yujinakayama/astrolabe
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems) 
Requires: rubygem(parser) >= 2.2.0.pre.3
Requires: rubygem(parser) < 3.0
BuildRequires: ruby(release)
BuildRequires: rubygems-devel 
BuildRequires: ruby 
# BuildRequires: rubygem(yard) => 0.8
# BuildRequires: rubygem(yard) < 1
# BuildRequires: rubygem(rspec) => 3.0
# BuildRequires: rubygem(rspec) < 4
# BuildRequires: rubygem(fuubar) => 2.0.0.rc1
# BuildRequires: rubygem(fuubar) < 2.1
# BuildRequires: rubygem(simplecov) => 0.7
# BuildRequires: rubygem(simplecov) < 1
# BuildRequires: rubygem(rubocop) => 0.24
# BuildRequires: rubygem(rubocop) < 1
# BuildRequires: rubygem(guard-rspec) >= 4.2.3
# BuildRequires: rubygem(guard-rspec) < 5.0
# BuildRequires: rubygem(guard-rubocop) => 1.0
# BuildRequires: rubygem(guard-rubocop) < 2
# BuildRequires: rubygem(ruby_gntp) => 0.3
# BuildRequires: rubygem(ruby_gntp) < 1
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
An object-oriented AST extension for Parser.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

# Remove developer-only files.
for f in .gitignore .rspec .rubocop.yml .travis.yml .yardopts Gemfile Guardfile Rakefile benchmark/.rubocop.yml benchmark/* spec/.rubocop.yml; do
  rm $f
  sed -i "s|\"$f\",||g" %{gem_name}.gemspec
done

%build
gem build %{gem_name}.gemspec
%gem_install

# remove unnecessary gemspec
rm .%{gem_instdir}/%{gem_name}.gemspec

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

# Run the test suite
%check
pushd .%{gem_instdir}

popd

%files
%{!?_licensedir:%global license %%doc}
%dir %{gem_instdir}
%license %{gem_instdir}/LICENSE.txt
%doc %{gem_instdir}/README.md
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_instdir}/CHANGELOG.md
%doc %{gem_docdir}
%exclude %{gem_instdir}/spec

%changelog
* Wed Jan 07 2015 Ken Dreyer <ktdreyer@ktdreyer.com> - 1.3.0-1
- Initial package
